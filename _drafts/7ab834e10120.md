---
layout: project
title: 'Splitvise - a user research case study'
caption: Perform a heuristic evaluation of Splitvise user interface
description: >

date: '28-08-2022'
image:
    path: /assets/img/projects/splitvise_case_study/cover.png
    srcset:
        1920w: /assets/img/projects/splitvise_case_study/cover.png
        960w:  /assets/img/projects/splitvise_case_study/cover.png
        480w:  /assets/img/projects/splitvise_case_study/cover.png
sitemap: false
---

1. this ordered seed list will be replaced by the toc
{:toc}

# Identifying and describing the interface

## Background

Splitwise is a US-based mobile based application with a single goal in mind i.e., keeping track of shared expenses with friends, family, and groups. Since its launch, the application has made transactions worth more than $90 billion (Reiff, 2021).

## Current interface

The current interface follows a single page navigation layout in Figure 13 with 5 tabs designated for different actions. Figure 13 shows the dashboard with 3 separate regions where the user can achieve their goals. Here, first region shows a button which can be used to add a new expense i.e., clicking this button will lead to the screen shown in Figure 14. The second region shows the list of added acquaintances (friends and groups) which can be seen in Figures 13 and 15 respectively. In the third region, users can click on activity to see their recent activities as shown in Figure 16. Moreover, Figure 17 shows how a user can settle an expense, this can either be recorded as an offline cash transaction or in some countries like India or US third party payment apps like PayTM can be used to pay the friends and family in-app (Bittner, 2017).

