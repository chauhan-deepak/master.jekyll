---
layout: page
title: Hello, I'm deepak chauhan 🙋🏽‍♂️
sitemap: false
no_drawer: true
---
📍 Sheffield, United Kingdom
<p class="designation_label" aria-label="Hi! I'm a developer">
  I'm a&nbsp;<span class="typewriter"></span>
</p>
<div style="text-align: justify">
<h2 class="about_description"> I've graduated in MSc Data Science from The University of Sheffield which is ranked #1 in information management for years 2020, 2021, 2022.</h2>
<h2 class="about_description"> I'm also an experienced software developer with 2+ years of experience working with Django, Serverless and SQL as well as ReactJS on client side.</h2>
</div>